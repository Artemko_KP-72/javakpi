package dao;

import  model.*;

import java.util.List;

public interface IDAO
{
    Pet getPet(Long id);
    List<Pet> getUserList();
}
