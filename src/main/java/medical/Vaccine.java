package medical;

import model.*;

import java.util.Date;
import java.util.List;

public class Vaccine
{
    enum Type {
        // Dogs
        CanineDistemperVirus,
        CanineAdenovirus_AKA_Hepatitis,
        CanineParvovirus,

        // Cats
        FelineLeukaemiaVirus_AKA_FeLV,
        ChlamydiaFelis,
        BordetellaBronchiseptica,
        FelineImmunodeficiencyVirus_AKA_FIV
    }

    Pet Owner;
    List<Pet> RecommendedToHave;
    Long Id;
    Date InjectionDate;
}
