package model;

import java.util.List;
import medical.Vaccine;

public class Pet
{

    private static Long globalId = 0L;

    private String name;
    private Integer age;
    private Long id;
    private List<Vaccine> vaccinationIds;

    public Pet(String name, Integer age)
    {
        this.name = name;
        this.age = age;
        this.id = ++globalId;
    }

    public String getName()
    {
        return this.name;
    }

    public Integer getAge()
    {
        return this.age;
    }

    public Long getId()
    {
        return this.id;
    }

    public List<Vaccine> getVaccinationIds()
    {
        return vaccinationIds;
    }

    public boolean MakeVaccination(Vaccine vaccine) {
        if (vaccinationIds.indexOf(vaccine) < 0)
            return false;
        else return vaccinationIds.add(vaccine);
    }

}
